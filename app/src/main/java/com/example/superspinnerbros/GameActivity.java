package com.example.superspinnerbros;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class GameActivity extends AppCompatActivity implements SensorEventListener {
    private GameView gameView;

    private SensorManager sensorManager;
    private String sensorType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        int ball1X = getExtra(intent.getStringExtra(MainActivity.EXTRA_BALL1_X), 100);
        int ball1Y = getExtra(intent.getStringExtra(MainActivity.EXTRA_BALL1_Y), 100);
        int ball1VelocityX = getExtra(intent.getStringExtra(MainActivity.EXTRA_BALL1_VELOCITY_X), 100);
        int ball1VelocityY = getExtra(intent.getStringExtra(MainActivity.EXTRA_BALL1_VELOCITY_Y), 100);

        int ball2X = getExtra(intent.getStringExtra(MainActivity.EXTRA_BALL2_X), 300);
        int ball2Y = getExtra(intent.getStringExtra(MainActivity.EXTRA_BALL2_Y), 100);
        int ball2VelocityX = getExtra(intent.getStringExtra(MainActivity.EXTRA_BALL2_VELOCITY_X), 100);
        int ball2VelocityY = getExtra(intent.getStringExtra(MainActivity.EXTRA_BALL2_VELOCITY_Y), 100);

        sensorType = intent.getStringExtra(MainActivity.EXTRA_SENSOR_TYPE);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        gameView = new GameView(this,
                new Vector2D(ball1X, ball1Y), new Vector2D(ball1VelocityX, ball1VelocityY),
                new Vector2D(ball2X, ball2Y), new Vector2D(ball2VelocityX, ball2VelocityY)
        );

        setContentView(gameView);
    }

    private int getExtra(String s, int defaultValue) {
        if (!s.equals("")) {
            return Integer.parseInt(s);
        }
        return defaultValue;
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(sensorType.equals("Gyroscope") ? Sensor.TYPE_GYROSCOPE : Sensor.TYPE_GRAVITY),
                SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            return;
        }

        gameView.onSensorEvent(sensorEvent);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}
