package com.example.superspinnerbros;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    static final String EXTRA_BALL1_Y = "EXTRA_BALL1_Y";
    static final String EXTRA_BALL1_X = "EXTRA_BALL1_X";
    static final String EXTRA_BALL1_VELOCITY_X = "EXTRA_BALL1_VELOCITY_X";
    static final String EXTRA_BALL1_VELOCITY_Y = "EXTRA_BALL1_VELOCITY_Y";

    static final String EXTRA_BALL2_X = "EXTRA_BALL2_X";
    static final String EXTRA_BALL2_Y = "EXTRA_BALL2_Y";
    static final String EXTRA_BALL2_VELOCITY_X = "EXTRA_BALL2_VELOCITY_X";
    static final String EXTRA_BALL2_VELOCITY_Y = "EXTRA_BALL2_VELOCITY_Y";
    static final String EXTRA_SENSOR_TYPE = "EXTRA_SENSOR_TYPE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startGame(View view) {
        Intent i = new Intent(this, GameActivity.class);

        i.putExtra(EXTRA_BALL1_X, ((EditText) findViewById(R.id.ball1X)).getText().toString());
        i.putExtra(EXTRA_BALL1_Y, ((EditText) findViewById(R.id.ball1Y)).getText().toString());
        i.putExtra(EXTRA_BALL1_VELOCITY_X, ((EditText) findViewById(R.id.ball1VelocityX)).getText().toString());
        i.putExtra(EXTRA_BALL1_VELOCITY_Y, ((EditText) findViewById(R.id.ball1VelocityY)).getText().toString());

        i.putExtra(EXTRA_BALL2_X, ((EditText) findViewById(R.id.ball2X)).getText().toString());
        i.putExtra(EXTRA_BALL2_Y, ((EditText) findViewById(R.id.ball2Y)).getText().toString());
        i.putExtra(EXTRA_BALL2_VELOCITY_X, ((EditText) findViewById(R.id.ball2VelocityX)).getText().toString());
        i.putExtra(EXTRA_BALL2_VELOCITY_Y, ((EditText) findViewById(R.id.ball2VelocityY)).getText().toString());

        i.putExtra(EXTRA_SENSOR_TYPE, ((Spinner) findViewById(R.id.sensorType)).getSelectedItem().toString());

        startActivity(i);
    }
}
