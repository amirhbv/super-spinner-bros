package com.example.superspinnerbros;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.View;

public class GameView extends View {

    private int width = 0;
    private int height = 0;
    private Ball ball1;
    private Ball ball2;

    private static final float NS2S = 1.0f / 1000000000.0f;
    private float eventTimestamp;

    private float theta;

    GameView(Context context) {
        super(context);
    }

    GameView(Context context,
             Vector2D ball1Position, Vector2D ball1Velocity,
             Vector2D ball2Position, Vector2D ball2Velocity
    ) {
        super(context);
        eventTimestamp = 0;
        theta = 0;

        this.ball1 = new Ball(ball1Position, ball1Velocity, 10, Color.RED);
        this.ball2 = new Ball(ball2Position, ball2Velocity, 50, Color.BLUE);

        final GameView that = this;
        this.post(new Runnable() {
            @Override
            public void run() {
                that.width = that.getMeasuredWidth();
                that.height = that.getMeasuredHeight();
            }
        });

    }

    void onSensorEvent(SensorEvent sensorEvent) {
        float[] values = sensorEvent.values;
        Vector2D acceleration;
        float verticalAcceleration;
        if (sensorEvent.sensor.getType() == Sensor.TYPE_GRAVITY) {
            acceleration = new Vector2D(-1 * values[0], values[1]);
            verticalAcceleration = values[2];

        } else if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            if (eventTimestamp == 0) {
                eventTimestamp = sensorEvent.timestamp;
                return;
            }

            final float dT = (sensorEvent.timestamp - eventTimestamp) * NS2S;
            if (dT < 0.001) {
                return;
            }

            eventTimestamp = sensorEvent.timestamp;

            float zw = values[2];
            if (Math.abs(zw) > 0.004) {
                theta += (zw * dT);
            }

            Log.d("THETA", String.format("%f - %f", theta, zw));

            acceleration = new Vector2D(Math.sin(theta), Math.sin(theta)).getMultipliedBy(SensorManager.GRAVITY_EARTH);
            verticalAcceleration = (float) (Math.cos(theta) * SensorManager.GRAVITY_EARTH);
        } else {
            return;
        }

        ball1.setAcceleration(acceleration);
        ball1.setGravitationCoefficient(verticalAcceleration);

        ball2.setAcceleration(acceleration);
        ball2.setGravitationCoefficient(verticalAcceleration);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        ball1.draw(canvas);
        ball2.draw(canvas);

        ball1.move(this.width, this.height);
        ball2.move(this.width, this.height);
        Ball.handleCollision(ball1, ball2);

        invalidate();
    }
}
