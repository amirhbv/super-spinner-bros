package com.example.superspinnerbros;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.Date;

class Ball {
    private static final int RADIUS = 25; //pixels
    private static final float KINETIC_FRICTION_COEFFICIENT = (float) 0.1;
    private static final float STATIC_FRICTION_COEFFICIENT = (float) 0.15;

    private static final float MS2S = 1.0f / 1000.0f;
    private long timestamp;

    private Vector2D position;
    private Vector2D velocity;
    private Vector2D acceleration;
    private float gravitationCoefficient;
    private int mass;
    private Paint paint;

    Ball(Vector2D position, Vector2D velocity, int mass, int color) {
        this.position = position;
        this.velocity = velocity;
        this.setAcceleration(new Vector2D());
        this.setGravitationCoefficient(SensorManager.GRAVITY_EARTH);
        this.mass = mass;

        this.paint = new Paint();
        this.paint.setColor(color);

        timestamp = new Date().getTime();
    }

    void setAcceleration(Vector2D acceleration) {
        this.acceleration = acceleration;
    }

    void setGravitationCoefficient(float gravitationCoefficient) {
        this.gravitationCoefficient = gravitationCoefficient;
    }

    void draw(Canvas canvas) {
        canvas.drawCircle(position.getX(), position.getY(), RADIUS, paint);
    }

    private void accelerate(float dT) {
        float ax = acceleration.getX();
        float ay = acceleration.getY();
        if (velocity.getX() == 0 && Math.abs(ax) < STATIC_FRICTION_COEFFICIENT * gravitationCoefficient) {
            ax = 0;
        }
        if (velocity.getY() == 0 && Math.abs(ay) < STATIC_FRICTION_COEFFICIENT * gravitationCoefficient) {
            ay = 0;
        }

        velocity.move(new Vector2D(ax, ay).getMultipliedBy(dT));

        Vector2D friction = velocity.getUnit().getMultipliedBy(-1 * KINETIC_FRICTION_COEFFICIENT * gravitationCoefficient);
        if (friction.getLength() >= velocity.getLength()) {
            velocity.setZero();
        } else {
            velocity.move(friction.getMultipliedBy(dT));
        }
    }

    void move(int width, int height) {
        long currentTime = new Date().getTime();
        final float dT = (currentTime - timestamp) * MS2S;

        if (dT < 0.001) {
            return;
        }

        Log.d("LOG", String.format("%s - %s - %s", position.toString(), velocity.toString(), acceleration.toString()));

        timestamp = currentTime;

        position.move(velocity.getMultipliedBy(dT).getSum(acceleration.getMultipliedBy((float) 0.5 * dT * dT)));
        accelerate(dT);

        float x = position.getX();
        float y = position.getY();
        float dx = velocity.getX() * dT;
        float dy = velocity.getY() * dT;

        if ((width > 0 && x + dx > width - RADIUS) || x + dx < RADIUS) {
            this.velocity.reverseX();
        }
        if ((height > 0 && y + dy > height - RADIUS) || y + dy < RADIUS) {
            this.velocity.reverseY();
        }
    }

    private boolean hasCollision(Ball other) {
        return this.position.getDistance(other.position) < 2 * RADIUS;
    }

    static void handleCollision(Ball ball1, Ball ball2) {
        if (!ball1.hasCollision(ball2)) {
            return;
        }

        Vector2D normalUnit = new Vector2D(ball1.position.getX() - ball2.position.getX(), ball1.position.getY() - ball2.position.getY()).getUnit();
        Vector2D tangentUnit = normalUnit.getVertical();

        float v1Tangent = tangentUnit.dotProduct(ball1.velocity);
        float v2Tangent = tangentUnit.dotProduct(ball2.velocity);

        float v1Normal = normalUnit.dotProduct(ball1.velocity);
        float v2Normal = normalUnit.dotProduct(ball2.velocity);

        int m1 = ball1.mass;
        int m2 = ball2.mass;

        float v1PrimeNormal = (v1Normal * (m1 - m2) + 2 * m2 * v2Normal) / (m1 + m2);
        float v2PrimeNormal = (v2Normal * (m2 - m1) + 2 * m1 * v1Normal) / (m1 + m2);

        ball1.velocity = new Vector2D(normalUnit.getMultipliedBy(v1PrimeNormal).getSum(tangentUnit.getMultipliedBy(v1Tangent)));
        ball2.velocity = new Vector2D(normalUnit.getMultipliedBy(v2PrimeNormal).getSum(tangentUnit.getMultipliedBy(v2Tangent)));
    }

}
