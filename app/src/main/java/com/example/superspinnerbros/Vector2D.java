package com.example.superspinnerbros;

class Vector2D {
    private float x, y;

    Vector2D() {
        this.setZero();
    }

    Vector2D(float x, float y) {
        this.x = x;
        this.y = y;
    }

    Vector2D(Vector2D vector2D) {
        this.x = vector2D.x;
        this.y = vector2D.y;
    }

    Vector2D(double x, double y) {
        this.x = (float) x;
        this.y = (float) y;
    }

    void move(Vector2D velocity) {
        this.x += velocity.x;
        this.y += velocity.y;
    }

    void setZero() {
        this.x = 0;
        this.y = 0;
    }

    float getX() {
        return x;
    }

    float getY() {
        return y;
    }

    void reverseX() {
        this.x = -1 * this.x;
    }

    void reverseY() {
        this.y = -1 * this.y;
    }

    float getDistance(Vector2D other) {
        float dx = this.x - other.x;
        float dy = this.y - other.y;
        return (float) Math.sqrt(dx * dx + dy * dy);
    }

    float getLength() {
        return (float) Math.sqrt(x * x + y * y);
    }

    Vector2D getUnit() {
        float len = getLength();
        if (len == 0) {
            return new Vector2D(0, 0);
        }
        return new Vector2D(x / len, y / len);
    }

    Vector2D getMultipliedBy(float scalar) {
        return new Vector2D(x * scalar, y * scalar);
    }

    Vector2D getSum(Vector2D other) {
        return new Vector2D(x + other.x, y + other.y);
    }

    Vector2D getVertical() {
        return new Vector2D(y, -x);
    }

    float dotProduct(Vector2D other) {
        return this.x * other.x + this.y * other.y;
    }

    boolean isZero() {
        return this.x == 0 && this.y == 0;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", this.x, this.y);
    }
}
